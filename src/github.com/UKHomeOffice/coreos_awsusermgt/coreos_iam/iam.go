package main

import (
  "fmt"
  "github.com/aws/aws-sdk-go/aws"
  "github.com/aws/aws-sdk-go/service/iam"
)

func getIamClient(cfg *aws.Config) *iam.IAM {
  isvc := iam.New(cfg)
  return isvc
}

func getUserKey(user string, svc *iam.IAM, k []*iam.SSHPublicKeyMetadata) string {
  encoding := "SSH"
  fmt.Printf("pub metadata: %+v", k)
  for _, key := range k {
    fmt.Printf("%+v", key)
    kresp, err := svc.GetSSHPublicKey(&iam.GetSSHPublicKeyInput{
      Encoding: aws.String(encoding),
      SSHPublicKeyId: key.SSHPublicKeyId,
      UserName: aws.String(user),
    })
    return *kresp.SSHPublicKeyBody
  }
}

func getKeyID(user string, svc *iam.IAM) ([]string, error) {
  keys := make([]string, 0)
  resp, err := svc.ListSSHPublicKeys(&iam.ListSSHPublicKeysInput{
    UserName: aws.String(user),
  })
  if err != nil {
    return keys, err
  }
  if len(resp.SSHPublicKeys) > 0 {
    getUserKey(user, svc, resp.SSHPublicKeys)
  }
  return keys, nil
}

func (u userMap) getKey(svc *iam.IAM) {
  for user, _ := range u {
    getKeyID(user, svc)
  }
}

func getIamUsers(r *iam.GetGroupOutput) []string {
  var users []string
  for _, user  := range r.Users {
    users = append(users, *user.UserName)
  }
  return users
}

func (u userMap) setIamUsers(svc *iam.IAM, g []string) {
  for _, grp := range g {
    groupInput := &iam.GetGroupInput{GroupName: aws.String(grp)}
    resp, err := svc.GetGroup(groupInput)
    if err != nil {
      fmt.Printf("error happened")
    }
    for _, user := range getIamUsers(resp) {
      u[user] = &userData{group: grp}
    }
  }
}
